package com.baig.codility;

public class FindEquilibrium {

	public static int findEquilibriumIndex(int[] arr) {
		int equi = 0;
		for (int i = 0; i < arr.length; i++) {
			equi = arr[i];
		}

		return -1;
	}

	public static int findEquilibriumIndex1(int[] A) {
		int a = -1;
		int i;
		int sum = 0;
		int lsum = 0, rsum = 0;
		int N = A.length;

		for (i = 0; i < N; i++) {
			sum += A[i];
		}

		for (i = 0; i < N; i++) {
			lsum = sum - rsum - A[i];
			if (lsum == rsum) {
				a = i;
			}
			rsum += A[i];
		}

		return a;
	}

	public static void main(String[] args) {
		int[] arr = {-7,1,5,2,-4,3};
		int equiIndex = findEquilibriumIndex1(arr);
		
		System.out.println("Equi Index: "+ equiIndex);
		
	}
}
