package com.baig.codility;

/**
 * 
 * A zero-indexed array A consisting of N different integers is given. The array
 * contains integers in the range [1..(N + 1)], which means that exactly one
 * element is missing.
 * 
 * Your goal is to find that missing element.
 * 
 * Write a function:
 * 
 * class Solution { public int solution(int[] A); }
 * 
 * that, given a zero-indexed array A, returns the value of the missing element.
 * 
 * For example, given array A such that:
 * 
 * A[0] = 2 A[1] = 3 A[2] = 1 A[3] = 5
 * 
 * the function should return 4, as it is the missing element.
 * 
 * Assume that:
 * 
 * N is an integer within the range [0..100,000]; the elements of A are all
 * distinct; each element of array A is an integer within the range [1..(N +
 * 1)].
 * 
 * Complexity:
 * 
 * expected worst-case time complexity is O(N); expected worst-case space
 * complexity is O(1), beyond input storage (not counting the storage required
 * for input arguments).
 * 
 */
public class MissingNumberInArray {

	public static int solution(int[] arr) {
		int sum = 0;
		int idx = -1;

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == 0) {
				idx = i;
			} else {
				sum += arr[i];
			}
		}

		// the total sum of numbers between 1 and arr.length.
		int total = ((arr.length + 1) * arr.length) / 2;

		System.out
				.println("Sum: " + sum + " Total: " + total
						+ " - missing number is: " + (total - sum)
						+ " at index " + idx);

		return total - sum;
	}

	public static int solution1(int[] arr) {
		int length = arr.length;

		int indexes = length + 1;
		int values = 0;

		for (int i = 0; i < length; i++) {
			indexes += i + 1;
			values += arr[i];
			
			System.out.println("Indexes: "+ indexes + " Values: "+ values);
		}

		int result = indexes - values;
		
		return result;
	}

	public static void main(String[] args) {
//		int[] arr = { 2, 3, 1, 5 };
		 int[] arr = { 10, 9, 3, 6, 4, 7, 8, 1, 2 };
		int missingNumber = solution1(arr);
		System.out.println("Missing Number: " + missingNumber);
	}
}
