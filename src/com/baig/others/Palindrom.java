package com.baig.others;

public class Palindrom {

	public static boolean solution1(String aString) {
		System.out.print(aString + ": ");

		char[] chars = aString.toCharArray();
		int j = chars.length - 1;
		int max = chars.length / 2;

		for (int i = 0; i < max; i++, j--) {
			if (chars[i] != chars[j]) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean solution2(String aString) {
	    String rev = new StringBuffer(aString).reverse().toString(); 

	    if(aString.equalsIgnoreCase(rev)){
	    	return true;
	    }
	    
	    return false;
	}
	
	public static void main(String[] args) {
		String source = "Amanaplanaca1nalPanama".toLowerCase();
		
		boolean isPalindrom = solution2(source);
		if(isPalindrom) {
			System.out.println("Palindrom Found");
		} else {
			System.out.println("No Palindrom Found");
		}
	}
}
